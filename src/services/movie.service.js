import http from "../http-common";
import authService from "./auth.service";

// Get one movie
const get = id => {
  return http.get(`/peliculas/${id}`, { headers: authService.authHeader() });
};

// Get movie list
const getAll = () => {
  return http.get("/peliculas", { headers: authService.authHeader() });
};

// Create a new movie
const create = data => {
  return http.post("/peliculas", data, { headers: authService.authHeader() });
};

// Update movie info
const update = (id, data) => {
  return http.put(`/peliculas/${id}`, data, { headers: authService.authHeader() });
};

// Delete a movie
const remove = id => {
  return http.delete(`/peliculas/${id}`, { headers: authService.authHeader() });
};

export default {
  getAll,
  get,
  create,
  update,
  remove
};
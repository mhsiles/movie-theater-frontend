import http from "../http-common";

// Get token
function authHeader() {
  const user = JSON.parse(localStorage.getItem('user'));

  if (user && user.accessToken) {
    return { 'x-access-token': user.accessToken };
  } else {
    return {};
  }
}

// Login
const login = data => {
  console.log(data);
  return http.post("/auth/signin", data)
    .then((response) => {
      if (response.data.accessToken) {
        // Set user information in local storage
        localStorage.setItem("user", JSON.stringify(response.data));
      }
      return response.data;
    });
}

// Logout
const logout = () => {
  localStorage.removeItem("user");
};

// Get user info from local
const getCurrentUser = () => {
  return JSON.parse(localStorage.getItem("user"));
};

export default {
  login,
  logout,
  getCurrentUser,
  authHeader
};
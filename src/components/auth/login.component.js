import React, { useState } from "react";
import AuthService from "../../services/auth.service";

const Login = (props) => {
  const initialState = {
    username: "",
    password: ""
  };

  // Hooks
  const [user, setUser] = useState(initialState);
  const [submitted, setSubmitted] = useState(false);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  const login = () => {

    var data = {
      username: user.username,
      password: user.password
    };

    AuthService.login(data)
      .then(() => {
        props.history.push("/peliculas");
        window.location.reload();
      })
      .catch(e => {
        console.log(e);
      });
  };

  return(
    <div className="submit-form">
      {submitted ? 
        (
          <div></div>
        )
      :
        (
          /* FORM */
          <div>
            <div className="form-group">
              <label htmlFor="username">Usuario</label>
              <input
                type="text"
                className="form-control"
                id="username"
                required
                value={user.username}
                onChange={handleInputChange}
                name="username"
              />
            </div>

            <div className="form-group">
              <label htmlFor="password">Contraseña</label>
              <input
                type="password"
                className="form-control"
                id="password"
                required
                value={user.password}
                onChange={handleInputChange}
                name="password"
              />
            </div>

            <button onClick={login} className="btn btn-success">
              Iniciar Sesión
            </button>
          </div>
        )
      }
    </div>
  )
};

export default Login;
import React, { useState, useEffect } from "react";
import MovieService from "../../services/movie.service";

const MoviesList = () => {
  const [movies, setMovieList] = useState([]);
  const [currentMovie, setCurrentMovie] = useState(null);
  const [updatedMovie, setupdatedMovie] = useState(false);
  const [currentIndex, setCurrentIndex] = useState(-1);

  // Get movies after view rendering
  useEffect(() => {
    getMovies();
  }, []);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setCurrentMovie({ ...currentMovie, [name]: value });
  };

  const getMovies = () => {
    MovieService.getAll()
      .then(response => {
        setMovieList(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const updateMovie = () => {
    var data = {
      name: currentMovie.name,
      year: currentMovie.year,
      director: currentMovie.director
    };

    MovieService.update(currentMovie.id, data)
    .then(response => {
      setupdatedMovie(true);
      refreshList();
    })
    .catch(e => {
      console.log(e);
    });
  };

  const deleteMovie = () => {
    var data = {
      name: currentMovie.name,
      year: currentMovie.year,
      director: currentMovie.director
    };

    MovieService.remove(currentMovie.id)
    .then(response => {
      setCurrentMovie(null);
      refreshList();
    })
    .catch(e => {
      console.log(e);
    });
  };

  const refreshList = () => {
    getMovies();
    setCurrentIndex(-1);
  };

  const setActiveMovie = (movie, index) => {
    setupdatedMovie(false);
    setCurrentMovie(movie);
    setCurrentIndex(index);
  };
  
  return (
    <div className="list row">
      <div className="col-sm-12">
        <h4>Lista de Películas</h4>

        <ul className="list-group">
          {movies &&
            movies.map((movie, index) => (
              <li
                className={
                  "list-group-item " + (index === currentIndex ? "active" : "")
                }
                onClick={() => setActiveMovie(movie, index)}
                key={index}
              >
                {movie.name}
              </li>
            ))}
        </ul>
      </div>
      <div className="col-sm-6 offset-sm-3 mt-3">
        <div className="card">
          <div className="card-body">
          <h4>Película</h4>
          {currentMovie ? (
              <div>
                <div className="form-group">
                  <label htmlFor="name">Nombre</label>
                  <input
                    type="text"
                    className="form-control"
                    id="name"
                    required
                    value={currentMovie.name}
                    onChange={handleInputChange}
                    name="name"
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="year">Año</label>
                  <input
                    type="number"
                    className="form-control"
                    id="year"
                    required
                    value={currentMovie.year}
                    onChange={handleInputChange}
                    name="year"
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="director">Director</label>
                  <input
                    type="text"
                    className="form-control"
                    id="director"
                    required
                    value={currentMovie.director}
                    onChange={handleInputChange}
                    name="director"
                  />
                </div>
                {updatedMovie ? 
                  <div>
                    <p className="mt-2">Película actualizada exitosamente.</p>
                  </div>
                  :
                  <button onClick={updateMovie} className="btn btn-success mt-2">
                    Actualizar Información
                  </button>
                }
                  <button onClick={deleteMovie} className="btn btn-danger mt-2">
                    Eliminar Película
                  </button>
              </div>
            ) : (
              <div>
                <p>Ninguna película seleccionada</p>
              </div>
            )}
          </div>
        </div>    
      </div>
    </div>
  );
};

export default MoviesList;
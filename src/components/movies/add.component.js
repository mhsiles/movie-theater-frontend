import React, { useState } from "react";
import MovieService from "../../services/movie.service";

const AddMovie = () => {
  const initialState = {
    id: null,
    name: "",
    year: 2000,
    director: ""
  };

  // Hooks
  const [movie, setMovie] = useState(initialState);
  const [submitted, setSubmitted] = useState(false);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setMovie({ ...movie, [name]: value });
  };

  const saveMovie = () => {

    var data = {
      name: movie.name,
      year: movie.year,
      director: movie.director
    };

    MovieService.create(data)
      .then(response => {
        setMovie({
          id: response.data.id,
          name: response.data.name,
          year: response.data.year,
          director: response.data.director
        });
        setSubmitted(true);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const newMovie = () => {
    setMovie(initialState);
    setSubmitted(false);
  };

  return(
    <div className="submit-form">
      {submitted ? 
        (
          <div>
            <h4>¡Película guardada exitosamente!</h4>
            <h5>ID: {movie.id}</h5>
            <h5>Película: {movie.name}</h5>
            <h5>Año estreno: {movie.year}</h5>
            <h5>Director: {movie.director}</h5>
            <button className="btn btn-success" onClick={newMovie}>
              AGREGAR PELÍCULA +
            </button>
          </div>
        )
      :
        (
          /* FORM */
          <div>
            <div className="form-group">
              <label htmlFor="name">Nombre</label>
              <input
                type="text"
                className="form-control"
                id="name"
                required
                value={movie.name}
                onChange={handleInputChange}
                name="name"
              />
            </div>

            <div className="form-group">
              <label htmlFor="year">Año</label>
              <input
                type="number"
                className="form-control"
                id="year"
                required
                value={movie.year}
                onChange={handleInputChange}
                name="year"
              />
            </div>

            <div className="form-group">
              <label htmlFor="director">Director</label>
              <input
                type="text"
                className="form-control"
                id="director"
                required
                value={movie.director}
                onChange={handleInputChange}
                name="director"
              />
            </div>

            <button onClick={saveMovie} className="btn btn-success">
              Guardar
            </button>
          </div>
        )
      }
    </div>
  )
};

export default AddMovie;
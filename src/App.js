import React, { useState, useEffect } from "react";
import { Switch, Route, Link, Redirect } from "react-router-dom";
import AuthService from "./services/auth.service";
import "bootstrap/dist/css/bootstrap.min.css";

// Movies Imports
import Login from "./components/auth/login.component";
import MoviesList from "./components/movies/list.component";
import AddMovie from "./components/movies/add.component";

function App(props) {

  let [logged, setLogged] = useState(false);

  const ProtectedRoute = ({
    component: Component,
    logged,
    setLogged,
    ...rest
  }) => {
    return (
      <Route
        {...rest}
        render={props => {
          if (!logged) {
            return (
              <Redirect
                to={{
                  pathname: "/login",
                  state: { flashInfo: "Please log in to continue." }
                }}
              />
            );
          } else {
            return <Component {...props} logged={logged} setLogged={setLogged} />;
          }
        }}
      />
    );
  };

  const logout = () => {
    AuthService.logout();
    window.location.reload();
  };

  useEffect(() => {
    if (window.localStorage.getItem("user")) {
      setLogged(true);
    } else {
      setLogged(false);
    }
  }, []);

  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/login" className="navbar-brand">
          Museo Cinematográfico
        </a>
        <div className="navbar-nav mr-auto">
          {
            logged ?
            <React.Fragment>
              <li className="nav-item">
                <Link to={"/peliculas"} className="nav-link">
                  Películas
                </Link>
              </li>
              <li className="nav-item">
                <Link to={"/agregar"} className="nav-link">
                  Agregar
                </Link>
              </li>
              <li className="nav-item">
                <Link onClick={logout} className="nav-link">
                  Cerrar Sesión
                </Link>
              </li>
            </React.Fragment>
            : null
          }
        </div>
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route exact path={["/", "/login"]} component={Login} />
          <ProtectedRoute logged={logged} exact path={["/peliculas"]} component={MoviesList} redirectTo='/login'/>
          <ProtectedRoute logged={logged} exact path="/agregar" component={AddMovie} redirectTo='/login'/>
        </Switch>
      </div>
    </div>
  );
}

export default App;
